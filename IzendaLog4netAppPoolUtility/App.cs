﻿using Microsoft.Extensions.Configuration;
using Microsoft.Web.Administration;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;

namespace IzendaLog4netAppPoolUtility
{
    public class App
    {
        #region Variables
        private readonly IConfigurationRoot _config;
        #endregion

        #region CTOR
        public App(IConfigurationRoot config) => _config = config;
        #endregion

        #region Methods
        /// <summary>
        /// Internal method to get Log4netFileDeleteInterval
        /// </summary>
        /// <returns></returns>
        internal int GetLog4netCleanupInterval()
        {
            var interval = _config.GetValue<int>("Log4netFileDeleteInterval");

            return interval;
        }

        /// <summary>
        /// Internal method to get Log4netFilesToKeep
        /// </summary>
        /// <returns></returns>
        internal int GetLog4neFilesToKeepThreshold()
        {
            var num = _config.GetValue<int>("Log4netFilesToKeep");

            return num;
        }

        /// <summary>
        /// Internal method to clean up old log4net files
        /// </summary>
        /// <returns></returns>
        internal void CleanupLog4netFiles()
        {
            var directory = _config.GetValue<string>("Log4netDirectory");

            var dirInfo = new DirectoryInfo(directory);
            if (!dirInfo.Exists)
            {
                throw new Exception("Unkown directory information");
            }

            var files = dirInfo.GetFiles();

            Array.Sort(files, delegate (FileInfo f1, FileInfo f2)
            {
                return f1.CreationTime.CompareTo(f2.CreationTime);
            });

            var filesCount = files.Length;
            var filesToKeep = GetLog4neFilesToKeepThreshold();

            if (filesCount > filesToKeep)
            {
                for (int i = filesToKeep; i <= filesCount - 1; i++)
                {
                    if (files[i].Name != "izenda-log.log")
                        files[i].Delete();
                }
            }
        }

        /// <summary>
        /// Internal method to recycle app pool
        /// </summary>
        /// <returns></returns>
        internal void RecycleAppPool()
        {
            var appPoolName = _config.GetValue<string>("AppPoolName");

            using (ServerManager iisManager = new ServerManager())
            {
                SiteCollection sites = iisManager.Sites;

                foreach (Site site in sites)
                {
                    if (site.Name == appPoolName)
                    {
                        iisManager.ApplicationPools[site.Applications["/"].ApplicationPoolName].Recycle();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Internal method to get ping interval
        /// </summary>
        /// <returns></returns>
        internal int GetPingInterval()
        {
            var interval = _config.GetValue<int>("PingInterval");

            return interval;
        }

        /// <summary>
        /// Internal method to test ping.
        /// Invoked only one time (intialization)
        /// </summary>
        /// <returns></returns>
        internal async Task<bool> TestPingAPI()
        {
            var url = _config.GetValue<string>("ApiUrl");

            var client = new HttpClient();
            HttpResponseMessage result;

            try
            {
                result = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, url));

                Console.WriteLine($"             Ping Status: {result.StatusCode} [{DateTime.Now}]");
                client = null;

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);

                return false;
            }
        }

        /// <summary>
        /// Internal method to conduct periodic ping job
        /// </summary>
        /// <returns></returns>
        internal async Task PingAPI()
        {
            var url = _config.GetValue<string>("ApiUrl");

            var client = new HttpClient();
            HttpResponseMessage result;

            try
            {
                result = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, url));
                Console.WriteLine($"Ping Status: {result.StatusCode} [{DateTime.Now}]");
                client = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        /// <summary>
        /// Internal method to get log4net lvl
        /// </summary>
        /// <returns></returns>
        internal string GetLog4netLogLevel()
        {
            var level = _config.GetValue<string>("Log4netLevel");

            return level;
        }

        /// <summary>
        /// Internal method to adjust Log4net level
        /// </summary>
        /// <returns></returns>
        internal void AdjustLog4netLogLevel()
        {
            var url = _config.GetValue<string>("Log4netConfigLocation");

            var doc = new XmlDocument();
            doc.Load(url);

            XmlNodeList aNodes = doc.SelectNodes("/log4net/appender/filter/levelMin");

            foreach (XmlNode aNode in aNodes)
            {
                XmlAttribute levelAttribute = aNode.Attributes["value"];

                if (levelAttribute != null)
                    levelAttribute.Value = GetLog4netLogLevel();
            }

            doc.Save(url);
        }
        #endregion
    }
}
