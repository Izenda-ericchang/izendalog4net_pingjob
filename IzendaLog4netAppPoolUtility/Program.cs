﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Timers;

namespace IzendaLog4netAppPoolUtility
{
    class Program
    {
        #region Variables
        public static IConfigurationRoot _configuration;
        #endregion

        #region Methods
        /// <summary>
        /// Main entry point
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static async Task Main(string[] args)
        {
            try
            {
                Console.WriteLine(@" _____                   _         _    _ _   _ _ _ _");
                Console.WriteLine(@"|_   _|                 | |       | |  | | | (_) (_) |");
                Console.WriteLine(@"  | |  _______ _ __   __| | __ _  | |  | | |_ _| |_| |_ _   _");
                Console.WriteLine(@"  | | |_  / _ \ '_ \ / _` |/ _` | | |  | | __| | | | __| | | |");
                Console.WriteLine(@" _| |_ / /  __/ | | | (_| | (_| | | |__| | |_| | | | |_| |_| |");
                Console.WriteLine(@"|_____/___\___|_| |_|\__,_|\__,_|  \____/ \__|_|_|_|\__|\__, |");
                Console.WriteLine(@"                                                         __/ |");
                Console.WriteLine(@"                                                        |___/ ");

                AddSpace();

                Console.WriteLine("                Updating Log4Net configuration...              ");
                Console.WriteLine("***************************************************************");
                Console.WriteLine("*  Log4net logging data volume will be adjusted as {0}      *", GetLog4netLevelInfo());
                Console.WriteLine("***************************************************************");
                MinimizeLog4netLevel();

                AddSpace();

                // invoke every 15 days cleanup
                var log4netInterval = GetLog4netFileDeleteInterval();
                Console.WriteLine("                 Start Log4net file monitor...                 ");
                Console.WriteLine("***************************************************************");
                Console.WriteLine("*    Log4net files will be cleaned up every {0} days interval  *", log4netInterval);
                Console.WriteLine("*           The recent {0} files would be not deleted.         *", GetLog4netFilesToKeepThreshold());
                Console.WriteLine("***************************************************************");
                var interval = new TimeSpan(log4netInterval, 0, 0, 0).TotalMilliseconds;
                var recyleTimer = new Timer(interval);
                recyleTimer.Elapsed += Log4netCleanup;
                recyleTimer.AutoReset = true;
                recyleTimer.Enabled = true;

                AddSpace();

                // initial ping
                Console.WriteLine("                    Ping to initialize API...                  ");
                var pingTest = await TestPing();
                if (!pingTest)
                {
                    recyleTimer.Elapsed -= Log4netCleanup;
                    Console.WriteLine("Failed to ping test. Please review your API url.");

                    Console.ReadKey();
                    Environment.Exit(0);
                }
                else
                    Console.WriteLine("                  API Ping test was successful!                ");

                AddSpace();

                int pingIntervalSetting = GetPingTestInterval();
                Console.WriteLine("***************************************************************");
                Console.WriteLine("*     Ping job will be called up every {0} minutes interval    *", pingIntervalSetting);
                Console.WriteLine("***************************************************************");

                // set up recurring ping
                var pingInterval = new TimeSpan(0, 0, pingIntervalSetting, 0).TotalMilliseconds;
                var pingTimer = new Timer(pingInterval);
                pingTimer.Elapsed += PingEvent;
                pingTimer.AutoReset = true;
                pingTimer.Enabled = true;

                AddSpace();
                Console.ReadKey();

                // un-subscribe events
                recyleTimer.Elapsed -= Log4netCleanup;
                pingTimer.Elapsed -= PingEvent;

                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ATTN: App stopped due to following error: {0}", ex.StackTrace);
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Call GetPingInterval service - retrieve ping interval.
        /// </summary>
        /// <returns></returns>
        private static int GetPingTestInterval()
        {
            var serviceProvider = GetServiceCollection();

            return serviceProvider.GetService<App>().GetPingInterval();
        }

        /// <summary>
        /// Call GetLog4netCleanupInterval service - retrieve log4net file clean up cycle.
        /// </summary>
        /// <returns></returns>
        private static int GetLog4netFileDeleteInterval()
        {
            var serviceProvider = GetServiceCollection();

            return serviceProvider.GetService<App>().GetLog4netCleanupInterval();
        }

        /// <summary>
        /// Call GetLog4neFilesToKeepThreshold - retrieve how many log4net files to be preserved after cleanup.
        /// </summary>
        /// <returns></returns>
        private static int GetLog4netFilesToKeepThreshold()
        {
            var serviceProvider = GetServiceCollection();

            return serviceProvider.GetService<App>().GetLog4neFilesToKeepThreshold();
        }

        /// <summary>
        /// Add space between each notification
        /// </summary>
        private static void AddSpace()
        {
            Console.WriteLine();
            Console.WriteLine();
        }

        /// <summary>
        /// Call test ping - intial ping job test
        /// </summary>
        /// <returns></returns>
        private static async Task<bool> TestPing()
        {
            var serviceProvider = GetServiceCollection();
            var result = await serviceProvider.GetService<App>().TestPingAPI();

            return result;
        }

        /// <summary>
        /// Subscribed event to do - keep invoking PingAPI call based on interval
        /// </summary>
        /// <returns></returns>
        private static async Task ProcessPingJob()
        {
            var serviceProvider = GetServiceCollection();

            await serviceProvider.GetService<App>().PingAPI();
        }

        /// <summary>
        /// Call CleanupLog4netFiles based on interval
        /// RecycleAppPool also called
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Log4netCleanup(object sender, ElapsedEventArgs e)
        {
            var serviceProvider = GetServiceCollection();

            serviceProvider.GetService<App>().CleanupLog4netFiles();
            serviceProvider.GetService<App>().RecycleAppPool();
        }

        /// <summary>
        /// ProcessPingJob called every tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static async void PingEvent(object sender, EventArgs e) => await ProcessPingJob();

        /// <summary>
        /// Call GetLog4netLogLevel to retrieve a target log4net level (e.g., ALL, DEBUG, INFOR, ERROR, ...)
        /// </summary>
        /// <returns></returns>
        private static string GetLog4netLevelInfo()
        {
            var serviceProvider = GetServiceCollection();

            return serviceProvider.GetService<App>().GetLog4netLogLevel();
        }

        /// <summary>
        /// Call AdjustLog4netLogLevel to Adjust Log4net level based on user settings
        /// RecycleAppPool also called to make changes implemented
        /// </summary>
        private static void MinimizeLog4netLevel()
        {
            var serviceProvider = GetServiceCollection();

            serviceProvider.GetService<App>().AdjustLog4netLogLevel();
            serviceProvider.GetService<App>().RecycleAppPool();
        }

        /// <summary>
        /// Service proverider build method
        /// </summary>
        /// <returns></returns>
        private static IServiceProvider GetServiceCollection()
        {
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            return serviceCollection.BuildServiceProvider();
        }

        /// <summary>
        ///  Configure service and inject it
        /// </summary>
        /// <param name="serviceCollection"></param>
        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            serviceCollection.AddSingleton(_configuration);
            serviceCollection.AddTransient<App>();
        }
        #endregion
    }
}
